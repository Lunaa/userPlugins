# Luna's [Vencord](https://codeberg.org/ven/cord) plugins

meant to be either cloned in src/userplugins or copied manually

**\*Note:** These plugins are created and tested on vencord's dev branch. Some plugins will break if used on main\*

<!-- markdownlint-disable-next-line header-increment -->
<!-- ### [How to install](https://youtu.be/8wexjSo8fNw) -->

## Plugins

### BannersEverywhere

Show user's banners behind their card in the member list

- Works with USRBG!

### FavouriteMedia

Allows you to add images to your gif picker

### BetterSed

Replaces discord's replace feature (`s/find/replace`) with full blown regex replacement

### Versions

Adds extra version info to the settings menu
Currently adds:

- Nodejs version
- Vencord git repo remote

## Dev Plugins

<!-- markdownlint-disable no-emphasis-as-header -->

_Plugins that are in review to add to official vencord_

### FakeSoundboard

Allows you to use soundboard without nitro

- Only works on web or vesktop

## WIP Plugins

**\*Note:** Any plugin here can break at any time. I will not provide support for them.\*

### EditUsers

Allows you to locally edit user's attributes, like their name

### Shiggy Clicker

Clone of shiggy clicker from the wip wailsinstaller
